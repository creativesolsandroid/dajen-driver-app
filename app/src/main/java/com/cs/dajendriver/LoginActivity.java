package com.cs.dajendriver;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by CS on 5/18/2017.
 */

public class LoginActivity extends AppCompatActivity {
    private EditText mPhone, mPassword;
    Button loginBtn;
    TextView forgotPwd;
    String response;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    GPSTracker gps;
    String userId;

    public static Double lat, longi;
    private static final String[] LOCATION_PERMS= {ACCESS_FINE_LOCATION};
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        mPhone = (EditText) findViewById(R.id.login_phone);
        mPassword = (EditText) findViewById(R.id.login_pwd);
        forgotPwd = (TextView) findViewById(R.id.forgot_pwd);
        loginBtn = (Button) findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = mPhone.getText().toString();
                String password = mPassword.getText().toString();
                if (phone.length() == 0) {
//                    if(language.equalsIgnoreCase("En")) {
                    mPhone.setError("Please enter Mobile number");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mEmail.setError("من فضلك أدخل رقم الجوال");
//                    }
                    mPhone.requestFocus();
                }else if(mPhone.length() != 9){
//                    if(language.equalsIgnoreCase("En")) {
                    mPhone.setError("Please enter valid Mobile Number");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mEmail.setError("من فضلك ادخل رقم جوال صحيح");
//                    }

                }else if (password.length() == 0) {
//                    if(language.equalsIgnoreCase("En")) {
                    mPassword.setError("Please enter Password");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mPassword.setError("من فضلك ادخل كلمة السر");
//                    }

                    mPassword.requestFocus();
                } else {
                    Log.e("TAG lat",""+lat);
                    Log.e("TAG","long"+longi);
                    new CheckLoginDetails().execute(Constants.LOGIN_URL+"966"+phone+"?psw="+password+"&dtoken="+SplashActivity.regid+"&lan=En");

                    getGPSCoordinates();
                    Log.e("TAG","mobileno"+phone);
                    Log.e("TAG","password"+password);
                    Log.e("TAG","token"+SplashActivity.regid);

                }
            }
        });



        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
//                startActivity(intent);
            }
        });
    }





    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = ProgressDialog.show(LoginActivity.this, "",
                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoginActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject jo1 = jo.getJSONObject("Success");
                                userId = jo1.getString("UserId");
                                String language = jo1.getString("Language");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                boolean isDriver = jo1.getBoolean("IsDriver");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");



                                if(isDriver) {
                                    try {
                                        JSONObject parent = new JSONObject();
                                        JSONObject jsonObject = new JSONObject();
                                        JSONArray jsonArray = new JSONArray();
                                        jsonArray.put("lv1");
                                        jsonArray.put("lv2");

                                        jsonObject.put("userId", userId);
                                        jsonObject.put("fullName", fullName);
                                        jsonObject.put("mobile", mobile);
                                        jsonObject.put("email", email);
                                        jsonObject.put("language", language);
                                        jsonObject.put("family_name", familyName);
                                        jsonObject.put("nick_name", nickName);
                                        jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                        jsonObject.put("user_details", jsonArray);
                                        parent.put("profile", jsonObject);
                                        Log.d("output", parent.toString());
                                        userPrefEditor.putString("user_profile", parent.toString());
                                        userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                        userPrefEditor.commit();

//                                    if(isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
                                        getGPSCoordinates();
//                                        Intent loginIntent = new Intent();

                                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(i);
                                        finish();
//                                    }else{
//                                        Intent loginIntent = new Intent(LoginActivity.this, VerifyRandomNumber.class);
//                                        loginIntent.putExtra("phone_number", mobile);
//                                        startActivityForResult(loginIntent, VERIFICATION_REQUEST);
//                                        finish();
//                                    }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                    // set title
                                    alertDialogBuilder.setTitle("Dajen");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("You are not assigned as a driver! Please contact operations team.")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();
                                }
                            }catch (JSONException je){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

//                                if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("Oregano");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("Invalid User")
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }else if(language.equalsIgnoreCase("Ar")){
                                // set title
                                alertDialogBuilder.setTitle("Dajen");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Invalid User")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
//                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(LoginActivity.this);
        JSONObject parent = new JSONObject();
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
//                lat = 24.805712;
//                longi = 46.693132;
                // Create a LatLng object for the current location
//                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                try {

                    JSONArray mainItem = new JSONArray();

                    JSONObject mainObj = new JSONObject();
                    mainObj.put("Latitude", lat);
                    mainObj.put("Longitude", longi);
                    mainObj.put("DriverId", userId);
                    mainObj.put("IsActive", true);

                    mainItem.put(mainObj);

                    parent.put("DriverLoc", mainItem);
                }catch (JSONException je){
                    je.printStackTrace();
                }
//                new UpdateLocationToServer().execute("http://85.194.94.241/oreganoservices/api/DriverApp?Latitude="+lat+"&Longitude="+longi+"&driverid="+userId);

                new UpdateLocationToServer().execute(parent.toString());
                Log.e("parent TAG", "log"+parent.toString());
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(LoginActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(LoginActivity.this, "Location permission denied", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class UpdateLocationToServer extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
//        ProgressDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
//            dialog = ProgressDialog.show(MainActivity.this, "",
//                    "Registering...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.live_url+"/api/DriverApp/UpdateDriverLocation");



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}

